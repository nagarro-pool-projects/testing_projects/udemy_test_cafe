import {Selector} from 'testcafe';


fixture `MyFixture`
.page `http://devexpress.github.io/testcafe/example`
.before(async t =>{
    // test setup goes here
    // await resetDatabase();
    // await seedTestData();
})
.beforeEach(async t =>{
    // run before each test
    await t.setTestSpeed(1);
})
.after(async t =>{
    // clean test data
    // logging and sending data to Monitoring Systems
})
.afterEach(async t =>{
    //Run after each test 
});


test.skip('send user name and validate if was sent',async(t)=>{
    // write the input value
    const inputUser =  Selector("#developer-name");
    await t.typeText(inputUser,"que pasa guapo");
    // check if the submit button is able
    const submitBtn = Selector("#submit-button");
    await t
    .expect(submitBtn.hasAttribute('disable')).notOk()
    // if btn is able click it!
    .click(submitBtn);
    // find title with some previus input text
    const title = Selector("#article-header");
    await t.expect(title.innerText).contains("guapo");
    await t.takeScreenshot({fullPage:true});
    // just wait 3 seconds before to close browser
    await t.wait(3000);

});

test.skip('actions',async t =>{
    const submitBtn = Selector("#submit-button");
    await t.click(submitBtn,{ options });
    await t.doubleClick(submitBtn,{ options });
    await t.rightClick(submitBtn),{ options };
    // drag element X eje Y eje
    await t.drag(submitBtn,100,20,{ options });
    await t.hover(submitBtn,{ options });
    await t.selectText('selectorHere');
    await t.typeText('selectorHere','new text here');
    await t.pressKey('enter');
    await t.navigateTo('url here');
    await t.takeElementScreenshot('Selector');
    await t.takeScreenshot({options});
    await t.resizeWindow(800,500);
});

test.skip('assertions', async t =>{
    const inputUser =  Selector("#developer-name");
    const submitBtn = Selector("#submit-button");

    await t.expect(inputUser.innerText).eql("hello","error message in case to fail", options);
    await t.expect(inputUser.innerText).notEql("lol");
    await t.expect(true).ok();
    await t.expect(false).notOk();
    await t.expect(inputUser.innerText).contains("lalo");
    await t.expect(inputUser.innerText).notContains("wepaa");
    // greater or less
    await t.expect(10).gt(9); // se espera que 10 sea mayor que 9
    await t.expect(10).gtet(10); // se espera que 10 sea mayor o igual que 10
    await t.expect(9).lt(10); // se espera que 9 sea menor que 10
    await t.expect(9).lte(9); // se espera que 9 sea menor o igual que 9
    // que este dentro de un rango de numeros
    await t.expect(15).within(10,20);
    await t.expect(9).notWithin(10,20); // que NO este dentro del rango de numeros
})

test('modificando un select por el primer valor encontrado en la lista', async(t)=>{
    const dropDownSelector = Selector('#preferred-interface');
    const option =  dropDownSelector.find('option');

    await t.click(dropDownSelector).click(option.withText('Both'));

    await t.click(dropDownSelector).click(option[0]);

    await t.click(dropDownSelector).click(option.withText('Both'));
});